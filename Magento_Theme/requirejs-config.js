var config = {

    paths: {
        owlcarousel: "Magento_Theme/js/owl.carousel",
        calculator: "Magento_Theme/js/calculator",
        calculator1: "https://www.mbank.net.pl/kalkulatory/calc/mbcalcLP.php?sprzedawca=30463394&pid=0",
    },
    shim: {
        'calculator': {
            'deps': ['jquery']
        }, 'calculator1': {
            'deps': ['calculator']
        }
    }
};