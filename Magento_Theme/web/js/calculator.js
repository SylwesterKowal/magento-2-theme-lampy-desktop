/*
 *  mBank Kalkulatory Rat - JavaScript file
 *  Copyright (C) 2011 Interpartner e-consulting, https://mbank.net.pl/
 */
var params = new Array();
var paramsLP = new Array();
var rates_array = new Array("3", "6", "10", "12", "18", "24", "36");
var re = /a|\u0105|b|c|\u0107|d|e|\u0119|f|g|h|i|j|k|l|\u0142|m|n|\u0144|o|\u00F3|p|q|r|s|\u015B|t|u|v|w|x|y|z|\u017C|\u017A|A|\u0104|B|C|\u0106|D|E|\u0118|F|G|H|I|J|K|L|\u0141|M|N|\u0143|O|\u00F2|P|Q|R|S|\u015A|T|U|V|W|X|Y|Z|\u017B|\u0179|@|#|~|`|\-|\%|\*|\^|\&|\(|\)|\+|\=|\[|\_|\]|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\jQuery/g;
var styleAddr = '';
var opiss = '';
var opiss2 = '';

function mbank_raty(target, kwota) {
    (function(e) {
        function r(t) {
            var n = t || window.event,
                r = [].slice.call(arguments, 1),
                i = 0,
                s = true,
                o = 0,
                u = 0;
            t = e.event.fix(n);
            t.type = "mousewheel";
            if (n.wheelDelta) {
                i = n.wheelDelta / 120
            }
            if (n.detail) {
                i = -n.detail / 3
            }
            u = i;
            if (n.axis !== undefined && n.axis === n.HORIZONTAL_AXIS) {
                u = 0;
                o = -1 * i
            }
            if (n.wheelDeltaY !== undefined) {
                u = n.wheelDeltaY / 120
            }
            if (n.wheelDeltaX !== undefined) {
                o = -1 * n.wheelDeltaX / 120
            }
            r.unshift(t, i, o, u);
            return (e.event.dispatch || e.event.handle).apply(this, r)
        }
        var t = ["DOMMouseScroll", "mousewheel"];
        if (e.event.fixHooks) {
            for (var n = t.length; n;) {
                e.event.fixHooks[t[--n]] = e.event.mouseHooks
            }
        }
        e.event.special.mousewheel = {
            setup: function() {
                if (this.addEventListener) {
                    for (var e = t.length; e;) {
                        this.addEventListener(t[--e], r, false)
                    }
                } else {
                    this.onmousewheel = r
                }
            },
            teardown: function() {
                if (this.removeEventListener) {
                    for (var e = t.length; e;) {
                        this.removeEventListener(t[--e], r, false)
                    }
                } else {
                    this.onmousewheel = null
                }
            }
        };
        e.fn.extend({
            mousewheel: function(e) {
                return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
            },
            unmousewheel: function(e) {
                return this.unbind("mousewheel", e)
            }
        })
    })(jQuery);
    // Predefined
    this.ilosc_rat_arr = new Array("10", "18", "36", "48");
    // Passed as arguments
    this.target = target;
    if (kwota == '' || kwota == 0.0) {
        this.kwota = 0.0;
    } else {
        this.kwota = round_decimals(kwota, 2);
    }

    this.calculator_1 = function Vertical() {
        if (params[target] == false) {
            return false;
        }
        var newClass = "";
        var ilosc_rat_loop = 2;
        if (window.seller) {
            newClass = window.seller;
            ilosc_rat_loop = 3;
        }
        var calc = "";
        calc += "<div class=\"mbank-calc " + newClass + "\" id=\"mbr_calc1_container\" > <div class=\"calc1\"><div class=\"top\"><!--<div class=\"lc\">&nbsp;</div><div class=\"rc\">&nbsp;</div>--></div><div class=\"content\"><div class=\"inner\"><div class=\"header\">&nbsp; </div><div class=\"rate\">Rata: <span id=\"mbc_rata_" + this.target + "\">0,00</span> Z&#321;</div><div class=\"rrso\">RRSO: <span id=\"mbc_rata_calc1\">" + rrso + "</span>%<sup>Zobacz<div>Jest to warto&#347;&#263; RRSO dla przyk&#322;adu reprezentatywnego. W zale&#380;no&#347;ci od zmiany parametr&#243;w po&#380;yczki RRSO b&#281;dzie inne.</div></sup></div><p class=\"choices\">Ilo\u015B\u0107 rat:<br />";

        for (i = 0; i <= ilosc_rat_loop; i++) {
            calc += "<input type=\"radio\" value=\"" + this.ilosc_rat_arr[i] + "\" onclick=\"przeliczRate('" + this.ilosc_rat_arr[i] + "','" + this.kwota + "','" + this.target + "','" + this.target + "');\" name=\"mbc_rb\" class=\"mb-calc-1\"";
            if (i == ilosc_rat_loop) calc += " checked=\"checked\"";
            calc += "> " + this.ilosc_rat_arr[i] + " rat <br />";
        }

        calc += "</p><p class=\"price\">Cena produktu:<br /><span id=\"mbc_cena_" + this.target + "\" class=\"big\">0,00</span> Z&#321;</p></div></div><div class=\"bottom\"><!--<div class=\"lc\">&nbsp;</div><div class=\"rc\">&nbsp;</div>--><p class=\"footer\"><a href=\"#\" title=\"" + opiss + "\" class=\"tooltip tooltip-calc-1\">Zobacz koszty po&#380;yczki</a></p></div></div>\n</div>\n";

        document.getElementById(this.target).innerHTML = calc;
        document.getElementById("mbc_cena_" + this.target).innerHTML = dotTocomma(this.kwota);
        przeliczRate(this.ilosc_rat_arr[ilosc_rat_loop], this.kwota, this.target, this.target);
    };

    this.calculator_2 = function Horizontal() {
        if (params[target] == false) {
            return false;
        }
        var newClass = "";
        var ilosc_rat_loop = 2;
        if (window.seller) {
            newClass = window.seller;
            ilosc_rat_loop = 3;
        }
        var calc = "";
        calc += "<div class=\"mbank-calc " + newClass + "\" id=\"mBank-calc-2\" class=\"horizontal\">\n\n<div class=\"center-calc-2\">\n<div class=\"header\"> &nbsp; </div>\n<form class=\"mb-calc-2\"><div class=\"mbc_text-calc-2\"><div class=\"mbc_bold-calc-2 mbc_red-calc-2\">Rata: <nobr><span id=\"mbc_rata_" + this.target + "\" class=\"mbc_big-calc-2\">0,00</span> Z&#321;</nobr></div><div class=\"mbc_bold-calc-2 mbc_red-calc-2\">RRSO: <nobr><span id=\"mbc_rata_calc2\" class=\" mbc_big-calc-2\">" + rrso + "</span>%</nobr><sup>Zobacz<div>Jest to warto&#347;&#263; RRSO dla przyk&#322;adu reprezentatywnego. W zale&#380;no&#347;ci od zmiany parametr&#243;w po&#380;yczki RRSO b&#281;dzie inne.</div></sup></div> </div>\n<div class=\"mbc_text-calc-2\">\n<nobr>\n<p class=\"mbc_gray-calc-2 rates\">Ilo\u015B\u0107 rat:</p>\n";
        for (i = 0; i <= ilosc_rat_loop; i++) {
            calc += "<p class=\"mbc_gray-calc-2\"><input class=\"mb-calc-2\" ";
            if (i == ilosc_rat_loop) calc += "checked=\"checked\"";
            calc += " type=\"radio\" name=\"mbc_rb\" onclick=\"przeliczRate('" + this.ilosc_rat_arr[i] + "','" + this.kwota + "','" + this.target + "','" + this.target + "');\" value=\"" + this.ilosc_rat_arr[i] + "\" />" + this.ilosc_rat_arr[i] + " rat</p>\n";
        }
        calc += "</nobr>\n</div><div class=\"mbc_text-calc-2\"><p class=\"mbc_bold-calc-2 mbc_gray-calc-2\">Cena produktu: <nobr><span id=\"mbc_cena_" + this.target + "\" class=\"mbc_big-calc-2\">0,00</span> Z&#321;</nobr></p> </div>\n<div class=\"spacer-calc-2\"></div>\n<div class=\"mbc_text-calc-2\"><p class=\"mbc_red-calc-2 l\"><a class=\"tooltip tooltip-calc-2\" href=\"#\" title=\"" + opiss + "\">Zobacz koszty po&#380;yczki</a></p></div>\n</form>\n</div>\n</div>\n";

        document.getElementById(this.target).innerHTML = calc;
        document.getElementById("mbc_cena_" + this.target).innerHTML = dotTocomma(this.kwota);
        przeliczRate(this.ilosc_rat_arr[ilosc_rat_loop], this.kwota, this.target, this.target);
    };

    this.calculator_3 = function noStyle() {
        if (params[target] == false) {
            return false;
        }
        // Taken from DB
        if (!ilo) {
            ilo = 12;
        }
        this.ilosc_rat = new Number(ilo);

        //przeliczRate(this.ilosc_rat,this.kwota,this.target);
        var in_brutto = getBrutto(this.ilosc_rat, this.kwota, this.target);
        // kwota nie powinna byc mniejsza niz 300 Z&#321;
        if (this.kwota >= 300) {
            var rata = round_decimals(((in_brutto * params[this.target][this.ilosc_rat]['opr']) / (12 * (1 - Math.pow((1 + (params[this.target][this.ilosc_rat]['opr'] / 12)), (-this.ilosc_rat))))), 2);

            if (!isNaN(rata)) {
                document.getElementById(this.target).innerHTML = '<p class="mBank-calc-3">' + this.ilosc_rat + ' x ' + dotTocomma(rata) + '</p>';
                document.getElementById(this.target).className += 'tooltip';
                document.getElementById(this.target).title = opiss;
            }
        }
    };

    this.calculator_4 = function LandingPage() {

        if (paramsLP == false) {
            return false;
        }
        if (this.kwota == 0.0) {
            this.kwota = parseFloat(2000.00);
        }
        var newClass = "";
        if (window.seller) {
            newClass = window.seller;
            rates_array.push("48");
        }
        var calc = "<div class=\"mbank-calc " + newClass + "\" id=\"mBank-calc-4\" class=\"horizontal-calc-4\">\n<div class=\"center-calc-4\">\n<form>\n<div class=\"field-calc-4\">\n<div class=\"input-calc-4\">  <div class=\"head1\"> Warto&#347;&#263; koszyka w sklepie </div>     <input onkeyup=setTimeout(\"checkNetto(\'" + this.target + "\')\",1000) type=\"text\" value=\"0,00\" id=\"mbc_netto_out\" name=\"mbc_netto_out\" /> <span class=\"currency\"> Z&#321; </span></div>\n<div class=\"label-calc-4\"><div id=\"mbc_netto\"></div></div>\n<div class=\"extras-calc-4\"></div>\n</div>\n<div class=\"field-calc-4\">\n<div class=\"input-calc-4\"><div class=\"head2\"> Kredyt b&#281;dziesz sp&#322;aca&#263; przez: </div><input class=\"mbc_small\"  onkeyup=\"setTimeout('checkIlRat(\\'" + this.target + "\\')',500)\" type=\"text\" value=\"36\" id=\"mbc_ilosc_rat_out\" name=\"mbc_ilosc_rat_out\" /> <span class=\"currency\">mc-y</span></div>\n<div class=\"label-calc-4\"><div id=\"mbc_ilosc_rat\"></div></div>\n<div class=\"extras-calc-4\"></div>\n</div>\n<div class=\"field-calc-4\">\n<div class=\"input-calc-4\"><div class=\"head3\"> Wysoko&#347;&#263; wp&#322;aty w&#322;asnej </div><input onkeyup=setTimeout(\"checkWklad(\'" + this.target + "\')\",1000) type=\"text\" value=\"0,00\" id=\"mbc_wklad_out\" name=\"mbc_wklad_out\" /> <span class=\"currency\"> Z&#321; </span></div>\n<div class=\"label-calc-4\"><div id=\"mbc_wklad\"></div></div>\n<div class=\"extras-calc-4\"></div>\n</div>\n</form>\n<div id=\"mbc_summary\"><span class=\"literal\">Wysoko&#347;&#263; twojej mBank Raty wyniesie:</span> <span id=\"mbc_rata_" + this.target + "\" class=\"amount\"></span>  </div>\n<div class=\"mbc_text-calc-4\"><p class=\"mbc_red-calc-4\"><a title=\"" + opiss + "\" href=\"#\" class=\"tooltip tooltip-calc-4\">Zobacz koszty po&#380;yczki</a></p></div>\n</div>\n<div class=\"bottom-calc-4\">\n</div>\n</div>\n";
        document.getElementById(this.target).innerHTML = calc;

        jQuery(function() {
            jQuery('#mbc_netto').slider({
                min: minLP,
                max: maxLP,
                value: kwota,
                animate: true,
                range: 'min',
                slide: function(event, ui) {
                    document.getElementById("mbc_netto_out").value = dotTocomma(round_decimals(ui.value, 2));
                    jQuery("#mbc_wklad").slider("option", "max", ui.value);
                    if (jQuery("#mbc_wklad").slider("option", "value") < ui.value) {
                        jQuery("#mbc_wklad").slider("option", "value", jQuery("#mbc_wklad").slider("option", "value"));
                        document.getElementById("mbc_wklad_out").value = dotTocomma(round_decimals(jQuery("#mbc_wklad").slider("option", "value"), 2));
                        przeliczRateLP(target);
                    } else {
                        jQuery("#mbc_wklad").slider("option", "value", (jQuery("#mbc_netto").slider("option", "value") - minLP));
                        document.getElementById("mbc_wklad_out").value = dotTocomma(round_decimals(ui.value - minLP, 2));
                        przeliczRateLP(target);
                    }
                },
                stop: function(event, ui) {
                    if (ui.value <= minLP) {
                        jQuery("#mbc_wklad").slider("option", "value", 0.00);
                        document.getElementById("mbc_wklad_out").value = dotTocomma("0.00");
                    }
                    przeliczRateLP(target);
                }
            });
            jQuery('#mbc_ilosc_rat').slider({
                min: 0,
                max: rates_array.length - 1,
                value: 7,
                animate: true,
                range: 'min',
                slide: function(event, ui) {
                    jQuery('#mbc_ilosc_rat').val('jQuery' + rates_array[ui.value] + ' - jQuery' + rates_array[rates_array.length - 1]);
                    document.getElementById("mbc_ilosc_rat_out").value = rates_array[ui.value];
                    przeliczRateLP(target);
                }
            });
            jQuery('#mbc_wklad').slider({
                min: 0,
                value: 0,
                animate: true,
                range: "min",
                slide: function(event, ui) {
                    jQuery("#mbc_wklad").slider("option", "max", (jQuery("#mbc_netto").slider("option", "value") - minLP));
                    document.getElementById("mbc_wklad_out").value = dotTocomma(round_decimals(ui.value, 2));
                    przeliczRateLP(target);
                },
                stop: function(event, ui) {
                    if (jQuery('#mbc_netto').slider("option", "value") <= minLP) {
                        jQuery("#mbc_wklad").slider("option", "value", 0.00);
                        document.getElementById("mbc_wklad_out").value = dotTocomma("0.00");
                    }
                    przeliczRateLP(target);
                }
            });
            jQuery("#mbc_wklad").slider("option", "max", (jQuery("#mbc_netto").slider("option", "value") - minLP));
            document.getElementById("mbc_netto_out").value = dotTocomma(round_decimals(jQuery("#mbc_netto").slider("option", "value"), 2));
            document.getElementById("mbc_ilosc_rat_out").value = rates_array[jQuery("#mbc_ilosc_rat").slider("option", "value")];
            document.getElementById("mbc_wklad_out").value = dotTocomma(round_decimals(jQuery("#mbc_wklad").slider("option", "value"), 2));
            przeliczRateLP(target);
        });
    };

    this.calculator_5 = function LinkToLandingPage(partner) {
        var newClass = "";
        var _this = this;
        if (window.seller) {
            newClass = " " + window.seller;
        }
        var calc = "<div title=\"" + opiss + "\" class=\"tooltip LinkToLandingPageWindow" + newClass + "\" onclick=\"\"></div>";

        //var calc = "<div title=\"" + opiss + "\" class=\"tooltip LinkToLandingPageWindow" + newClass + "></div>";
        document.getElementById(this.target).innerHTML = calc;
        jQuery('#' + this.target).unbind('click').bind('click', function(e) {
            e.preventDefault();
            LinkToLandingPageWindow(_this.kwota, partner, '1005', newClass);
        });


    };

    this.calculator_6 = function LinkToLandingPage(partner) {
        var newClass = "";
        var _this = this;
        if (window.seller) {
            newClass = " " + window.seller;
        }
        //var calc = "<div title=\"" + opiss + "\" class=\"tooltip LinkToLandingPageWindow2" + newClass + "\" onclick=\"var lp = new mrLP({button: '.LinkToLandingPageWindow',sprzedawca:" + partner + ",kwota:" + this.kwota + "});\"></div>";
        var calc = "<div title=\"" + opiss + "\" class=\"tooltip LinkToLandingPageWindow2" + newClass + "\" onclick=\"\"></div>";
        document.getElementById(this.target).innerHTML = calc;
        jQuery('#' + this.target).unbind('click').bind('click', function(e) {
            e.preventDefault();
            LinkToLandingPageWindow(_this.kwota, partner, '1006', newClass);
        });
    };
    //piotr chwirot kalkulator mbank 5.0
    this.calculator_7 = function() {
        var noteHandler = function(el, note) {
            jQuery(el).hover(
                function() {
                    note.fadeIn();
                }
            );
            note.find('.close').bind('click', function(e) {
                note.fadeOut();
            });
        };
        if (paramsLP == false) {
            return false;
        };
        if (this.kwota == 0.0) {
            this.kwota = parseFloat(2000.00);
        };
        var initBasketVal = 0;
        if (gup('kwota', window.location.href)) {
            if (!isNaN(parseFloat(gup('kwota', window.location.href)))) {
                initBasketVal = parseFloat(gup('kwota', window.location.href));
            } else {
                initBasketVal = 1000;
            }

        } else {
            initBasketVal = 1000;
        };
        var initUserPayment = minLP;
        var initMonths = rates_array[0];
        var interval = 100;
        var monthsOptions = '';
        var rssoLiteral = 'Zobacz koszty po&#380;yczki';
        for (var i = 0; i < rates_array.length; i++) {
            if (i == 0) {
                monthsOptions += '<option>' + rates_array[i] + ' miesiĂÄce</option>'
            } else {
                monthsOptions += '<option>' + rates_array[i] + ' miesi&#281;cy</option>'
            };
        };
        var calc = styleAddr + '<div class="calcContainer mbank-calc"><div class="calculator"><div class="calc-head"><p>Oblicz rat&#281;</p></div><div class="calc-data"><div class="basket-value"><span class="literal">Warto&#347;&#263; koszyka:</span><span class="inputs"><input type="text" name="mbc_netto_out" class="ui" id="mbc_netto_out" value="' + initBasketVal + '"><span class="customize-buttons"><span class="plus">+</span><span class="minus">-</span></span></span></div><div class="user-payment"><span class="literal">Wp&#322;ata w&#322;asna:</span><span class="inputs"><input type="text" name="mbc_wklad_out" class="ui" id="mbc_wklad_out" value="' + initUserPayment + '"><span class="customize-buttons"><span class="plus">+</span><span class="minus">-</span></span></span></div><div class="payment-cycle"><input type="hidden" name="mbc_ilosc_rat_out" id="mbc_ilosc_rat_out" value="' + initMonths + '"><span class="literal">Okres sp&#322;aty:</span><select class="ui" id="rates_count">' + monthsOptions + '</select></div></div><div class="result"><p>Twoja miesi&#281;czna rata: <span type="text" class="result-value" id="result-value" >500 z&#322;</span></p></div><div class="calc-footer"><div class="continue"><a href="#" id="continue-btn" class="button red next">Doko&#324;cz zakupy <span>&nbsp;</span></a></div><div class="noteBtn"><div class="note second"><p>' + opiss + '</p><span class="curret"></span><span class="close">x</span></div><p id="calcRsso"><span>&nbsp;</span>' + rssoLiteral + '</p></div></div></div></div>';

        function gup(name, addres) {
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regexS = "[\\?&]" + name + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(addres);
            if (results == null) return "";
            else return results[1];
        };
        var rssoNoteHandler = function(el, note) {
            jQuery(el).hover(
                function() {
                    note.fadeIn();
                }
            );
            note.find('.close').bind('click', function(e) {
                note.fadeOut();
            });
        };
        var userPaymentValid = function() {
            if (parseFloat(jQuery('#mbc_netto_out').val()) >= maxLP) {
                jQuery('#mbc_netto_out').val(maxLP);
            } else if (parseFloat(jQuery('#mbc_netto_out').val()) <= minLP) {
                jQuery('#mbc_netto_out').val(minLP);
            };
            maxUserPayment = parseFloat(jQuery('#mbc_netto_out').val()) - minLP;
            if (parseFloat(jQuery('#mbc_wklad_out').val()) > maxUserPayment) {
                jQuery('#mbc_wklad_out').val(round_decimals(maxUserPayment, 2));
            };
            jQuery('#mbc_netto_out').val(round_decimals(jQuery('#mbc_netto_out').val(), 2));
            jQuery('#mbc_wklad_out').val(round_decimals(jQuery('#mbc_wklad_out').val(), 2));
        }
        var valueUp = function(input, val) {
            var currentVal = parseFloat(input.val());
            if (currentVal < maxLP) {
                input.val(currentVal + val);
            } else {
                input.val(maxLP);
            }
            userPaymentValid();
            przeliczRate50(target);
        };
        var valueDown = function(input, val) {
            var currentVal = parseFloat(input.val());
            if (currentVal - val >= minLP) {
                input.val(currentVal - val);
            } else {
                input.val(minLP);

            };
            userPaymentValid();
            przeliczRate50(target);
        };
        var userPaymentUp = function(input, val) {
            var currentVal = parseFloat(input.val());
            input.val(currentVal + val);

            userPaymentValid();
            przeliczRate50(target);
        };
        var userPaymentDown = function(input, val) {
            var currentVal = parseFloat(input.val());
            if (currentVal > interval - 0.01) {
                input.val(currentVal - val);
            } else {
                input.val(0);
            }
            userPaymentValid();
            przeliczRate50(target);
        };
        var digitsValidate = function(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) {
                    theEvent.preventDefault();
                }
            };
        };
        target.append(calc);
        var maxUserPayment = parseFloat(jQuery('#mbc_netto_out').val()) - minLP;
        jQuery('#mbc_wklad_out, #mbc_netto_out').keypress(function(evt) {
            digitsValidate(evt);
        });
        jQuery('#mbc_wklad_out').change(function() {
            maxUserPayment = parseFloat(jQuery('#mbc_netto_out').val()) - minLP;
            if (parseFloat(jQuery(this).val()) > maxUserPayment) {
                jQuery(this).val(round_decimals(maxUserPayment, 2));
            };
            przeliczRate50(target);
        });
        jQuery('#mbc_netto_out, #mbc_wklad_out, #rates_count').change(function() {
            userPaymentValid();
            przeliczRate50(target);
        });
        var timeout;
        jQuery('.basket-value .inputs .customize-buttons .plus').mousedown(function() {
            var _this = jQuery(this)
            timeout = setInterval(function() {
                valueUp(_this.parent().parent().find('input'), interval);

            }, 100);

            return false;
        });
        jQuery(document).mouseup(function() {
            clearInterval(timeout);
            return false;
        });
        jQuery('.basket-value .inputs .customize-buttons .minus').bind('mousedown', function(e) {
            var _this = jQuery(this)
            timeout = setInterval(function() {
                valueDown(_this.parent().parent().find('input'), interval);

            }, 100);

            return false;
        });

        jQuery('.user-payment .inputs .customize-buttons .plus').bind('mousedown', function(e) {
            var _this = jQuery(this)
            timeout = setInterval(function() {
                userPaymentUp(_this.parent().parent().find('input'), interval);

            }, 100);

            return false;
        });
        jQuery('.user-payment .inputs .customize-buttons .minus').bind('mousedown', function(e) {
            var _this = jQuery(this)
            timeout = setInterval(function() {
                userPaymentDown(_this.parent().parent().find('input'), interval);

            }, 100);

            return false;
        });
        noteHandler(jQuery('#calcRsso'), jQuery('.note.second'));
        jQuery('#continue-btn').bind('click', function(e) {
            window.close();
        });
        przeliczRate50(target);
    };
    this.calculator_8 = function(partner, type) {
        var calcType = type || 1;
        var CustomScroll = function(params) {
            this.scrollHolder = params.scrollHolder;
            this.initHeight = params.scrollHolder.outerHeight()
            this.scrollHolderWidth = params.scrollHolderWidth || 300;
            this.scrollHolderHeight = params.scrollHolderHeight || 300;
            this.scrollBar = '<span class="scrollBarHolder"><span class="scrollBar"></span></span>';
            this.scrollBarSelector; // = jQuery(this.scrollHolder.find('.scrollBar'));
            this.scrollBarHolderSelector; // = jQuery(this.scrollHolder.find('.scrollBarHolder'));
            this.scrollProcentPos = 0;
            this.contentWraper;
            this.scrollStep = 2;
            this.scroll;
            this.initDefaults = {
                scope: this,
                init: function(scope) {
                    //var style = document.createElement('style');
                    //style.type = 'text/css';
                    //style.innerHTML = '.cs-scrollBarHolder { background: #5f5; width: 10px; height: 100%; position: absolute; top: 0; right: 0;} .cs-scrollBar { background: #ffa; width:10px; height:30px; position: absolute; top: 0; left: 0; cursor: pointer;} .cs-inner-container {position: absolute; top: 0; left: 0;}';
                    //document.getElementsByTagName('head')[0].appendChild(style);
                    if (scope.scrollHolder.css('position').toString() == 'static') {
                        scope.scrollHolder.css('position', 'relative');
                    }
                    scope.scrollHolder.find('.scrollBarHolder').addClass('cs-scrollBarHolder');
                    scope.scrollHolder.find('.scrollBar').addClass('cs-scrollBar');
                    scope.scrollBarSelector = scope.scrollHolder.find('.scrollBar');
                    scope.scrollBarHolderSelector = scope.scrollHolder.find('.scrollBarHolder');
                    scope.contentWraper = scope.scrollHolder.find('.cs-inner-container');
                    if (scope.initHeight <= scope.scrollHolder.outerHeight()) {
                        scope.scrollHolder.find('.scrollBarHolder').css('display', 'none');
                        scope.scrollHolder.find('.scrollBar').css('display', 'none');
                    }
                }
            };
            this.drag = function() {
                var scope = this;
                var countPos = function() {
                    var sbPos = parseInt(scope.scrollBarSelector.css('top'), 10);
                    var sbParentHeight = scope.scrollBarHolderSelector.outerHeight();
                    scope.scrollProcentPos = sbPos / (sbParentHeight - parseInt(scope.scrollBarSelector.outerHeight(), 10)); //procent value of scrollbar position
                    var contentPos = parseInt(scope.contentWraper.outerHeight(), 10) - parseInt(scope.scrollHolder.outerHeight(), 10);
                    contentPos = parseInt(contentPos * scope.scrollProcentPos, 10) * -1;
                    if (contentPos >= 0) {
                        contentPos = 0;
                    }
                    return contentPos;
                };
                this.scrollBarSelector.draggable({
                    disabled: false,
                    containment: "parent",
                    axis: "y",
                    //opacity: params.opacity,
                    drag: function() {
                        scope.contentWraper.css('top', countPos());
                    },
                    stop: function() {}
                });
                this.scrollHolder.mousewheel(function(event, delta) {
                    var nextStep = scope.scrollStep;
                    var prevStep = scope.scrollStep;
                    event.preventDefault();
                    if (delta < 0 && parseInt(scope.scrollBarSelector.css('top'), 10) <= parseInt(scope.scrollBarHolderSelector.outerHeight(), 10) - parseInt(scope.scrollBarSelector.outerHeight(), 10)) {
                        if (parseInt(scope.scrollBarSelector.css('top'), 10) + scope.scrollStep >= parseInt(scope.scrollBarHolderSelector.outerHeight(), 10) - parseInt(scope.scrollBarSelector.outerHeight(), 10)) {
                            nextStep = parseInt(scope.scrollBarHolderSelector.outerHeight(), 10) - parseInt(scope.scrollBarSelector.css('top'), 10) - parseInt(scope.scrollBarSelector.outerHeight(), 10);
                        } else {
                            nextStep = scope.scrollStep;
                        }
                        scope.scrollBarSelector.css('top', parseInt(scope.scrollBarSelector.css('top'), 10) + nextStep);
                    }
                    if (delta > 0 && parseInt(scope.scrollBarSelector.css('top'), 10) > 0) {
                        if (parseInt(scope.scrollBarSelector.css('top'), 10) - scope.scrollStep <= 0) {
                            prevStep = parseInt(scope.scrollBarSelector.css('top'), 10);
                        } else {
                            prevStep = scope.scrollStep;
                        }
                        scope.scrollBarSelector.css('top', parseInt(scope.scrollBarSelector.css('top'), 10) - prevStep);
                    }
                    scope.contentWraper.css('top', countPos());
                });
            };
            this.init = (function(scope) {
                scope.scrollHolder.css('width', scope.scrollHolderWidth);
                scope.scrollHolder.css('height', scope.scrollHolderHeight);
                scope.scrollHolder.css('overflow', 'hidden');
                scope.scrollHolder.css('padding-right', 10);
                jQuery(scope.scrollHolder).wrapInner('<div class="cs-inner-container"></div>');


                scope.scrollHolder.append(scope.scrollBar);
                scope.initDefaults.init(scope);
                scope.drag();


                jQuery('.cs-inner-container').width(parseInt(scope.scrollHolderWidth, 10));
            })(this);
        };

        partner = partner || '';
        if (params[this.target] == false || params[this.target] == undefined) {
            params[this.target] = paramsLP;
        }

        var calc = "";
        var newClass = "";
        var ilosc_rat_new = 36;
        if (window.seller) {
            newClass = window.seller;
            ilosc_rat_new = 48;
        }
        calc += '<div class="calc7 mbank-calc ' + newClass + '">';
        calc += '<span class="logo"></span>'

        if (calcType == 1) {
            calc += '<p class="kwota">od <span id="mbc_rata_calc7"></span> <span>z&#322;</span></p>';
            calc += '<p>miesi&#281;cznie</p>';
        } else {
            calc += '<p class="number-of-rates"><strong>' + ilosc_rat_new + '</strong> rat x</p>';
            calc += '<p class="kwota"><span id="mbc_rata_' + this.target + '"></span> <span>z&#322;</span></p>';
        }
        calc += '<p class="legalNoteLabel">Zobacz koszty po&#380;yczki &raquo;</p>';

        if (calcType == 1) {
            calc += '<a href="#" class="button red count type1">Oblicz<span></span></a>';
        } else {
            calc += '<a href="#" class="button red count type2">Oblicz<span></span></a>';
        }
        calc += '<div class="legalNote">';
        calc += '<div class="content"><p>' + opiss + '</p></div>';
        calc += '</div>';
        calc += '<span class="arrow"></span></div>';

        document.getElementById(this.target).innerHTML = calc;
        this.scroll = new CustomScroll({
            scrollHolder: jQuery('#' + this.target + ' .legalNote'),
            scrollHolderWidth: 260,
            scrollHolderHeight: 109
        });

        //, '+'#'+this.target+' .arrow'
        jQuery('#' + this.target + ' .legalNoteLabel').hover(function(e) {
                jQuery('#' + target + ' .legalNote, ' + '#' + target + ' .arrow').fadeIn();
            },
            function() {});
        jQuery('body').bind('click', function() {
            jQuery('.calc7 .legalNote, .calc7 .arrow').fadeOut();
        });
        przeliczRate(ilosc_rat_new, this.kwota, this.target, this.target);
        jQuery('.button.red.count.type1').unbind('click').bind('click', function(e) {
            e.preventDefault();
            //window.open('http://www.mbank.net.pl/mraty_1/index.html?sprzedawca=' + partner + '&kwota=' + kwota + '', 'title', 'directories=0,location=1,menubar=0,resizable=0,scrollbars=0,status=0,toolbar=0,width=800,height=550');
            LinkToLandingPageWindow(kwota, partner, '1007', newClass);
        });
        jQuery('.button.red.count.type2').unbind('click').bind('click', function(e) {
            e.preventDefault();
            //window.open('http://www.mbank.net.pl/mraty_1/index.html?sprzedawca=' + partner + '&kwota=' + kwota + '', 'title', 'directories=0,location=1,menubar=0,resizable=0,scrollbars=0,status=0,toolbar=0,width=800,height=550');
            LinkToLandingPageWindow(kwota, partner, '1008', newClass);
        });
    };
}

function przeliczRate50(target) {
    var il_rat = parseInt(jQuery('#rates_count option:selected').text());
    var in_brutto = parseFloat(getBruttoLP(il_rat));
    var rata = round_decimals(((in_brutto * paramsLP[il_rat]['opr']) / (12 * (1 - Math.pow((1 + (paramsLP[il_rat]['opr'] / 12)), (-il_rat))))), 2);
    jQuery('#result-value').html(round_decimals(rata, 2));
}

function LinkToLandingPageWindow(kwota, partner, id, isNew) {
    //http://www.mbank.pl/mbank_raty/

    var addr = window.location.href;
    addr = addr.split('/');
    //if (addr[0].toString() != 'https:') {
    adres = 'https://www.mbank.net.pl/adserwer/kreacje/' + id + ',,' + partner + '.html';
    //}
    jQuery.ajax({
        url: adres,
        type: 'get'
    });

    var left = (screen.width / 2) - (800 / 2);
    var top = (screen.height / 2) - (550 / 2);
    if (isNew) {
        window.open('https://www.mbank.net.pl/mraty_1/index.html?sprzedawca=' + partner + '&kwota=' + kwota + '', 'title', 'directories=0,location=1,menubar=0,resizable=0,scrollbars=0,status=0,toolbar=0,width=800,height=550,left=' + left + ',top=' + top);
    } else {
        window.location.href = 'http://www.mbank.pl/mbank_raty/internetowy/?sprzedawca=' + partner + '&kwota=' + kwota;
    }
}

function przeliczRate(ilosc_rat, kwota, target, target) {
    var ilosc_rat = new Number(ilosc_rat);
    var in_brutto = getBrutto(ilosc_rat, kwota, target);
    //alert(params[target][ilosc_rat]['opr']);
    //alert(in_brutto);
    var rata = round_decimals(((in_brutto * params[target][ilosc_rat]['opr']) / (12 * (1 - Math.pow((1 + (params[target][ilosc_rat]['opr'] / 12)), (-ilosc_rat))))), 2);

    if (!isNaN(rata)) {
        document.getElementById("mbc_rata_" + target).innerHTML = dotTocomma(rata);
    }
}

function getBrutto(ilosc_rat, kwota, target) {
    var ilosc_rat = new Number(ilosc_rat);
    var in_kwota = kwota;
    var out_brutto = round_decimals((in_kwota * (1 + params[target][ilosc_rat]['pro'])) / (1 - (ilosc_rat * params[target][ilosc_rat]['ube']) - (ilosc_rat * (params[target][ilosc_rat]['ube'] * params[target][ilosc_rat]['pro']))), 2);
    return out_brutto;
}

/* -- Landing Page Start -- */
function getNettoLP() {
    var in_netto = parseFloat(valSplitter(document.getElementById("mbc_netto_out").value));
    var in_wklad = parseFloat(valSplitter(document.getElementById("mbc_wklad_out").value));

    var out_netto = in_netto - in_wklad;

    return out_netto;
}

function getBruttoLP(ilosc_rat) {
    var il_rat = new Number(ilosc_rat);
    var in_netto = new Number(getNettoLP());

    var out_brutto = round_decimals((in_netto * (1 + paramsLP[ilosc_rat]['pro'])) / (1 - (il_rat * paramsLP[ilosc_rat]['ube']) - (il_rat * (paramsLP[ilosc_rat]['ube'] * paramsLP[ilosc_rat]['pro']))), 2);

    return out_brutto;
}

function przeliczRateLP(target) {
    var il_rat = parseInt(document.getElementById("mbc_ilosc_rat_out").value);
    var in_brutto = parseFloat(getBruttoLP(il_rat));
    var rata = round_decimals(((in_brutto * paramsLP[il_rat]['opr']) / (12 * (1 - Math.pow((1 + (paramsLP[il_rat]['opr'] / 12)), (-il_rat))))), 2);

    if (!isNaN(rata)) {
        document.getElementById("mbc_rata_" + target).innerHTML = dotTocomma(rata);
    }
}


function checkNetto(target) {
    var val = valSplitter(document.getElementById("mbc_netto_out").value.replace(re, ""));
    if (val.length != 0) {
        if (val.length > 2) {
            if (val.length == parseInt(0)) {
                document.getElementById("mbc_netto_out").value = dotTocomma(round_decimals(minLP, 2));
                jQuery("#mbc_netto").slider("option", "value", minLP);
                document.getElementById("mbc_wklad_out").value = dotTocomma("0.00");
                jQuery("#mbc_wklad").slider("option", "value", "0.00");
                jQuery("#mbc_wklad").slider("option", "max", round_decimals(val2, 2));
            } else if (parseFloat(val) > parseFloat(jQuery("#mbc_netto").slider("option", "max"))) {
                document.getElementById("mbc_netto_out").value = dotTocomma(round_decimals(maxLP, 2));
                jQuery("#mbc_netto").slider("option", "value", maxLP);
            } else if ((parseFloat(val) < parseFloat(0.00)) || (parseFloat(val) < minLP)) {
                document.getElementById("mbc_netto_out").value = dotTocomma(round_decimals(minLP, 2));
                jQuery("#mbc_netto").slider("option", "value", minLP);
            } else {
                jQuery("#mbc_netto").slider("option", "value", val);
                document.getElementById("mbc_netto_out").value = dotTocomma(round_decimals(val, 2));
                if ((val - minLP) <= parseFloat(0.00)) {
                    jQuery("#mbc_wklad").slider("option", "value", parseFloat(0.00));
                    document.getElementById("mbc_wklad_out").value = dotTocomma("0.00");
                } else if (round_decimals(document.getElementById("mbc_wklad_out").value, 2) > val - minLP) {
                    jQuery("#mbc_wklad").slider("option", "max", val - minLP);
                    jQuery("#mbc_wklad").slider("option", "value", val - minLP);
                    document.getElementById("mbc_wklad_out").value = dotTocomma(round_decimals(val - minLP, 2));
                } else {
                    jQuery("#mbc_wklad").slider("option", "max", round_decimals(val - minLP, 2));
                    jQuery("#mbc_wklad").slider("option", "value", jQuery("#mbc_wklad").slider("option", "value"));
                    document.getElementById("mbc_wklad_out").value = dotTocomma(round_decimals(jQuery("#mbc_wklad").slider("option", "value"), 2));
                }
            }
            przeliczRateLP(target);
        }
    } else {
        document.getElementById("mbc_netto_out").value = dotTocomma(round_decimals(minLP, 2));
        jQuery("#mbc_netto").slider("option", "value", minLP);
    }
}

function checkIlRat(target) {

    var val = document.getElementById("mbc_ilosc_rat_out").value.replace(re, "");
    if (val.length > 0) {
        if (parseInt(val) < 3) {
            document.getElementById("mbc_ilosc_rat_out").value = "3";
            jQuery("#mbc_ilosc_rat").slider("option", "value", 0);
        } else if (parseInt(val) > 36) {
            document.getElementById("mbc_ilosc_rat_out").value = "36";

            jQuery("#mbc_ilosc_rat").slider("option", "value", 6);
        } else {
            if (parseInt(val) < 6) {
                jQuery("#mbc_ilosc_rat").slider("option", "value", 0);

                document.getElementById("mbc_ilosc_rat_out").value = "3";
            } else if (parseInt(val) < 10) {
                jQuery("#mbc_ilosc_rat").slider("option", "value", 1);

                document.getElementById("mbc_ilosc_rat_out").value = "6";
            } else if (parseInt(val) < 12) {
                jQuery("#mbc_ilosc_rat").slider("option", "value", 2);

                document.getElementById("mbc_ilosc_rat_out").value = "10";
            } else if (parseInt(val) <= 15) {
                jQuery("#mbc_ilosc_rat").slider("option", "value", 3);

                document.getElementById("mbc_ilosc_rat_out").value = "12";
            } else if (parseInt(val) <= 21) {
                jQuery("#mbc_ilosc_rat").slider("option", "value", 4);

                document.getElementById("mbc_ilosc_rat_out").value = "18";
            } else if (parseInt(val) <= 24) {
                jQuery("#mbc_ilosc_rat").slider("option", "value", 5);

                document.getElementById("mbc_ilosc_rat_out").value = "24";
            } else if (parseInt(val) > 24) {
                jQuery("#mbc_ilosc_rat").slider("option", "value", 6);
                document.getElementById("mbc_ilosc_rat_out").value = "36";
            }
        }
        przeliczRateLP(target);
    }
}

function sleep(time) {
    time = time * 200;
    var start = (new Date()).getTime();
    while (true) {
        alarm = (new Date()).getTime();
        if (alarm - start > time) {
            break;
        }
    }
}

function checkWklad(target) {
    var val = valSplitter(document.getElementById("mbc_wklad_out").value.replace(re, ""));
    var val2 = valSplitter(document.getElementById("mbc_netto_out").value.replace(re, ""));
    if (val.length > parseInt(0)) {
        if (val.length > parseInt(2)) {
            if (val.length == parseInt(0) || parseFloat(val2) <= minLP) {
                document.getElementById("mbc_wklad_out").value = dotTocomma("0.00");
                jQuery("#mbc_wklad").slider("option", "value", "0.00");
                jQuery("#mbc_wklad").slider("option", "max", round_decimals(val2, 2));
            } else if (parseFloat(val) > parseFloat(val2 - minLP)) {
                document.getElementById("mbc_wklad_out").value = dotTocomma(round_decimals(val2 - minLP, 2));
                jQuery("#mbc_wklad").slider("option", "max", val2 - minLP);
                jQuery("#mbc_wklad").slider("option", "value", val2 - minLP);
            } else if (parseFloat(val) < parseFloat(0.00)) {
                document.getElementById("mbc_wklad_out").value = dotTocomma(round_decimals(val2 - minLP, 2));
                jQuery("#mbc_wklad").slider("option", "max", val2 - minLP);
                jQuery("#mbc_wklad").slider("option", "value", val2 - minLP);
            } else {
                jQuery("#mbc_wklad").slider("option", "max", val2 - minLP);
                jQuery("#mbc_wklad").slider("option", "value", val);
                document.getElementById("mbc_wklad_out").value = dotTocomma(round_decimals(val, 2));
            }
            przeliczRateLP(target);
        }
    } else {
        document.getElementById("mbc_wklad_out").value = dotTocomma(round_decimals(0.00, 2));
        jQuery("#mbc_wklad").slider("option", "max", parseFloat(0.00));
        jQuery("#mbc_wklad").slider("option", "value", parseFloat(0.00));
    }
}
/* -- Landing Page End -- */

function valSplitter(old_val) {
    var temp = "";
    var first_split = old_val.split(" ");
    var second_split;
    if (first_split[1] != null) {
        for (i = 0; i < first_split.length; i++) {
            temp = temp + first_split[i];
        }
    } else {
        temp = first_split[0];
    }
    second_split = temp.split(",");
    if (second_split[1] != null) {
        var new_val = second_split[0] + "." + second_split[1];
    } else {
        var new_val = second_split[0];
    }
    return new_val;
}

function dotTocomma(old_val) {
    var temp = old_val.split(".");
    var new_val = temp[0] + "," + temp[1];
    return new_val;
}

function round_decimals(original_number, decimals) {
    var result1 = original_number * Math.pow(10, decimals);
    var result2 = Math.round(result1);
    var result3 = result2 / Math.pow(10, decimals);
    return pad_with_zeros(result3, decimals);
}

function pad_with_zeros(rounded_value, decimal_places) {
    var value_string = rounded_value.toString();
    var decimal_location = value_string.indexOf(".");
    if (decimal_location == -1) {
        decimal_part_length = 0;
        value_string += decimal_places > 0 ? "." : "";
    } else {
        decimal_part_length = value_string.length - decimal_location - 1;
    }
    var pad_total = decimal_places - decimal_part_length;
    if (pad_total > 0) {
        for (var counter = 1; counter <= pad_total; counter++) value_string += "0";
    }
    return value_string;
}

function getElementsByClassName(oElm, strTagName, strClassName) {
    var arrElements = (strTagName == "*" && oElm.all) ? oElm.all : oElm.getElementsByTagName(strTagName);
    var arrReturnElements = [];
    strClassName = strClassName.replace(/-/g, "\-");
    var oRegExp = new RegExp("(^|\s)" + strClassName + "(\s|jQuery)");
    var oElement;
    for (var i = 0; i < arrElements.length; i++) {
        oElement = arrElements[i];
        if (oRegExp.test(oElement.className)) {
            arrReturnElements.push(oElement);
        }
    }
    return (arrReturnElements.length > 0 ? arrReturnElements : false);
}

function getId(eid) {
    var el = false;
    try {
        el = document.getElementById(eid);
    } catch (e) {}
    return el;
}

function addEvent(obj, fnc, act) {
    if (obj.addEventListener) obj.addEventListener(act, fnc, false);
    else if (obj.attachEvent) obj.attachEvent('on' + act, fnc);
    return false;
}

this.tooltip = function() {
    /* CONFIG */
    xOffset = 10;
    yOffset = 20;
    // these 2 variable determine popup's distance from the cursor
    // you might want to adjust to get the right result
    /* END CONFIG */
    jQuery(".tooltip").hover(function(e) {
            this.t = this.title;
            this.title = "";
            jQuery("body").append("<p id='tooltip'>" + this.t + "</p>");
            jQuery("#tooltip")
                .css("top", (e.pageY - xOffset) + "px")
                .css("left", (e.pageX + yOffset) + "px")
                .fadeIn("fast");
        },
        function() {
            this.title = this.t;
            jQuery("#tooltip").remove();
        });
    jQuery(".tooltip").mousemove(function(e) {
        jQuery("#tooltip")
            .css("top", (e.pageY - xOffset) + "px")
            .css("left", (e.pageX + yOffset) + "px");
    });
};

var mrLP = function(params) {
    var sprzedawca = '';
    var kwota = '';
    if (params.sprzedawca) {
        sprzedawca = params.sprzedawca;
    }
    if (params.kwota) {
        kwota = params.kwota;
    }
    var lpLayer = '<div id="mrClose" style="position: absolute; width: 630px; height: 60px; background: none; left: 0; right: 0; margin: 0 auto; top: 3%; z-index: 2; cursor: pointer;"></div><div id="mrLPcontainer" style="position:absolute; width: 100%; height: 100%; left: 0; top: 0; z-index: 1;"><iframe id="mrIframe" src="http://mbank.net.pl/mraty_1/?sprzedawca=' + sprzedawca + '&kwota=' + kwota + '" style="width:800px; height: 850px; margin: 0 auto; z-index: 100;position: absolute;top: 3%;left: 0;right: 0; border: none;"></iframe> <div id="mrLpCover" style="background: #000; opacity: 0.4; cursor:pointer; position:absolute; width: 100%; height: 100%; left: 0; top: 0; z-index: 99"></div></div>';

    if (params.button) {
        jQuery(params.button).bind('click', function(e) {
            e.preventDefault();
            jQuery('#mrLPcontainer, #mrLpCover, #mrClose').remove();
            jQuery('body').append(lpLayer);
            jQuery('#mrLpCover, #mrClose').unbind('click').bind('click', function(e) {
                jQuery('#mrLPcontainer, #mrLpCover, #mrClose').remove();
            })
        });
    }
}

var mrCalc = function(params) {
    var urlParams;
    (window.onpopstate = function() {
        var match,
            pl = /\+/g, // Regex for replacing addition symbol with a space
            search = /([^&=]+)=?([^&]*)/g,
            decode = function(s) {
                return decodeURIComponent(s.replace(pl, " "));
            },
            query = window.location.search.substring(1);

        urlParams = {};
        while (match = search.exec(query))
            urlParams[decode(match[1])] = decode(match[2]);
        console.log(urlParams);
    })();
    var paramsString = '';
    var objIter = 0;
    var paramsLength = Object.keys(urlParams).length;
    if (paramsLength > 0) {
        paramsString += '?';
    }
    for (var key in urlParams) {
        var obj = urlParams[key];
        paramsString += key + '=' + urlParams[key];
        if (objIter < paramsLength - 1) {
            paramsString += '&';
        }
        objIter++;
    }

    this.init = function() {
        /*
        jQuery.ajax({
            url: 'https://www.mbank.net.pl/mbpartnertest/kalkulatory/calc/Toplayer-landing-res2.htm' + paramsString,
            type: 'GET',
            dataType: 'html'
        }).done(function(html) {
            jQuery(params.container).empty().append(html);
        }).fail(function() {
            //alert('error');
        })
        */
        var addr = window.location.href;
        var adres = ''
        adres = 'https://www.mbank.net.pl/kalkulatory/calc';
        if(params.kwota) kwota = params.kwota;
        else kwota = 300;
        jQuery('<iframe style="border:none; width:550px; height:420px;" id="mrCalcIF" src="' + adres + '/Toplayer-landing-res2.htm?sprzedawca='+sprzedawca+'&kwota='+kwota+'"></iframe>').appendTo(jQuery(params.container));
    }
}

// starting the script on page load
jQuery(document).ready(function() {
    tooltip();
});